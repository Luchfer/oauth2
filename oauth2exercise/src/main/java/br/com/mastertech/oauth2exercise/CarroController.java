package br.com.mastertech.oauth2exercise;


import br.com.mastertech.oauth2exercise.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarroController {

    @GetMapping("/{modelo}")
    public Carro create(@PathVariable String modelo, @AuthenticationPrincipal Usuario usuario) {
        Carro carro = new Carro();
        carro.setModelo(modelo);
        carro.setDono(usuario.getName());
        return carro;
    }
}
