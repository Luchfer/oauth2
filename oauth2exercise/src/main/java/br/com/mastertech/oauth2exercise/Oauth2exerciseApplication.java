package br.com.mastertech.oauth2exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2exerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2exerciseApplication.class, args);
	}

}
