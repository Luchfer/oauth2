package br.com.mastertech.contactlist.repository;

import br.com.mastertech.contactlist.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    List<Contact> findByIdUsuario(Integer idUsuario);
}
