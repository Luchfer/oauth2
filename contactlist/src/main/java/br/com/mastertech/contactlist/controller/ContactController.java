package br.com.mastertech.contactlist.controller;

import br.com.mastertech.contactlist.exception.ContactsNotFoundException;
import br.com.mastertech.contactlist.model.Contact;
import br.com.mastertech.contactlist.model.ContactMapper;
import br.com.mastertech.contactlist.request.ContactRequest;
import br.com.mastertech.contactlist.response.ContactResponse;
import br.com.mastertech.contactlist.security.Usuario;
import br.com.mastertech.contactlist.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contact")
public class ContactController {
    @Autowired
    private ContactService contactService;
    @Autowired
    private ContactMapper contactMapper;

    @PostMapping
    public ContactResponse createContact(@RequestBody @Valid ContactRequest request, @AuthenticationPrincipal Usuario usuario){
        Contact contact = contactService.create(contactMapper.toContact(request,usuario));
        return contactMapper.toResponse(contact);
    }

    @GetMapping
    public List<ContactResponse> getContact(@AuthenticationPrincipal Usuario usuario){
        List<Contact> listData = contactService.getContacts(usuario);
        if(listData.isEmpty()){
            throw new ContactsNotFoundException();
        }
        return contactMapper.toResponseList(listData);
    }

}
