package br.com.mastertech.contactlist.request;

import javax.validation.constraints.NotNull;

public class ContactRequest {

    @NotNull
    private String nomeContato;
    @NotNull
    private String telefoneContato;



    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getTelefoneContato() {
        return telefoneContato;
    }

    public void setTelefoneContato(String telefoneContato) {
        this.telefoneContato = telefoneContato;
    }
}
