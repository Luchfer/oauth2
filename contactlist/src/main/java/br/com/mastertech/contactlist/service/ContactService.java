package br.com.mastertech.contactlist.service;

import br.com.mastertech.contactlist.model.Contact;
import br.com.mastertech.contactlist.repository.ContactRepository;
import br.com.mastertech.contactlist.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {
    @Autowired
    private ContactRepository contactRepository;

    public Contact create(Contact dataContact){
      return dataContact = contactRepository.save(dataContact);
    }

    public List<Contact> getContacts(Usuario dataUser){
        return contactRepository.findByIdUsuario(dataUser.getId());
    }
}
