package br.com.mastertech.contactlist.model;

import br.com.mastertech.contactlist.request.ContactRequest;
import br.com.mastertech.contactlist.response.ContactResponse;
import br.com.mastertech.contactlist.security.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContactMapper {

    public Contact toContact(ContactRequest request, Usuario usuario){
        Contact contact = new Contact();
        contact.setIdUsuario(usuario.getId());
        contact.setNomeContato(request.getNomeContato());
        contact.setTelefoneContato(request.getTelefoneContato());
        return contact;
    }

    public ContactResponse toResponse(Contact contact){
        ContactResponse response = new ContactResponse();
        response.setId(contact.getId());
        response.setIdUsuario(contact.getIdUsuario());
        response.setNomeContato(contact.getNomeContato());
        response.setTelefoneContato(contact.getTelefoneContato());
        return response;
    }
    public List<ContactResponse> toResponseList(List<Contact> contacts){
        List<ContactResponse> responses = new ArrayList<ContactResponse>();
        contacts.forEach(contact ->{
            ContactResponse data = new ContactResponse();
            data.setId(contact.getId());
            data.setIdUsuario(contact.getIdUsuario());
            data.setNomeContato(contact.getNomeContato());
            data.setTelefoneContato(contact.getTelefoneContato());
            responses.add(data);
        });
        return responses;
    }
}
