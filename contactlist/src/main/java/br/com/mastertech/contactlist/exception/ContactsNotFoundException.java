package br.com.mastertech.contactlist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "This user don't have any contact")
public class ContactsNotFoundException extends RuntimeException{

}
